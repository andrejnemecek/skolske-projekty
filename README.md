# Školské projekty
Výber niekoľko relevantných školských projektov:

## Bezpečnosť webových serverov
Dokument: [**bezpecnost_webovych_serverov.pdf**](https://gitlab.com/andrejnemecek/skolske-projekty/-/blob/master/bezpecnost_webovych_serverov.pdf)

Zabezpečil som Node.js webovú aplikáciu, ktorá je nasadená na Ubuntu serveri, kde beží v Docker kontajneri. Pred aplikáciou je postavený Nginx webový server, ktorý slúži ako reverse proxy.

 - Zabezpečenie linux servera
 - Konfigurácia WAF - ModSecurity pre Nginx
 - Nasadenie Let's Encrypt certifikátu
 - Konfigurácia HTTP hlavičiek


## Tímový projekt
**Dokumentácia k projektu: http://team.animalrescue.sk/**

Vytvorili sme webovú aplikáciu na ochranu týraných zvierat. Výsledok nie je dokonalý, ale **veľa sme sa naučili**. Použili sme technológie:

 - Vue.js
 - TypeScript
 - Flask
 - PostgreSQL
 - Azure DevOps
 - atď...

## Vyhľadávanie informácií

### 1. zadanie - Elasticsearch
Dokument: [**elasticsearch.pdf**](https://gitlab.com/andrejnemecek/skolske-projekty/-/blob/master/elasticsearch.pdf)
 - Zber údajov z reálnej stránky (spravodajský web)
 - Spracovanie dát a import do Elasticsearch
 - Tvorba zložitejších dopytov v Elasticsearch

### 2. zadanie - Recommender pre eshop
Dokument: [**recommender-eshop.pdf**](https://gitlab.com/andrejnemecek/skolske-projekty/-/blob/master/recommender-eshop.pdf)
 - Spracovanie reálnych dát z eshopu (nákup, pozretie tovaru)
 - Vytvorenie recommender-a pre odporúčanie tovaru konkrétnym zákazníkom

## Detekcia spamových emailov
Dokument: [**klasifikator-spam.pdf**](https://gitlab.com/andrejnemecek/skolske-projekty/-/blob/master/klasifikator-spam.pdf)

Vytvorenie klasifikátora na detekciu spamových e-mailov.
